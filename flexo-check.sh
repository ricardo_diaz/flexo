#!/usr/bin/env bash

default_ifacename="wlan0"

while getopts ":h" opt; do
    case $opt in
	h)
	    echo "Usage: flexo-check [-h] [<interface>]"
	    echo "  <interface>: interface name, default: $default_ifacename"
	    exit 0
	    ;;
	\?)
	    echo "Invalid option: -$OPTARG" >&2
	    exit 2
	    ;;
    esac
done

iface="${1:-$default_ifacename}"
ifacemon="${iface}mon"

airmon-ng stop $iface &>/dev/null
airmon-ng start $iface &>/dev/null
if [ "$?" -ne 0 ]
then
    echo "Cannot start wireless interface \"$iface\" in monitor mode)"
    exit 3
fi

echo "Press Ctrl+C to exit";sleep 2
airodump-ng $ifacemon

